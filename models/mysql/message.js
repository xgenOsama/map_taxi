module.exports = {
    insert: async function (data) {
        let [message,ignored] = await sql.query("INSERT INTO messages (travel_id,driver_id,rider_id,type,content,created_at) VALUES (?,?, ?, ?, ?,?)", [
            data.travel_id,
            data.driver_id,
            data.rider_id,
            data.type,
            data.content,
            data.created_at
        ]);
        let [msg,ig] = await sql.query("SELECT * FROM messages WHERE id = ?", [message.insertId]);
        console.log(msg);
        return msg;
    },
    getByTravel: async function(travelId){
        let [messages,ignored] = await sql.query("SELECT * FROM messages WHERE travel_id = ? ORDER BY created_at DESC", [travelId]);
        return messages;
    }
};

