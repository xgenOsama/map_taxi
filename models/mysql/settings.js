module.exports = {
    getSettings: async function () {
        let [result, ignored] = await sql.query("SELECT * FROM general_settings order by id desc limit 1");
        //result.map(x=>x.location = [x.location.y,x.location.x]);
        return result[0];
    },
    insert: async function (data) {
        let [result, ignored] = await sql.query("INSERT INTO general_settings (max_drivers,max_distance,minimum_payment_request,percent_company,cash_payment_commission,rider_min_ver_ios,driver_min_ver_ios,rider_min_ver_android,driver_min_ver_android) VALUES (?,?,?,?,?,?,?,?,?)"
            , [
                data.max_drivers,
                data.max_distance,
                data.minimum_payment_request,
                data.percent_company,
                data.cash_payment_commission,
                data.rider_min_ver_ios,
                data.driver_min_ver_ios,
                data.rider_min_ver_android,
                data.driver_min_ver_android
            ]);
        return result[0];
    },
    update: async function (data) {
        let [id, ign] = await sql.query("SELECT id FROM general_settings order by id desc limit 1");
        let [result, ignored] = await sql.query("UPDATE general_settings SET max_drivers = ? ,max_distance = ?,minimum_payment_request = ?,percent_company = ?,cash_payment_commission = ?,rider_min_ver_ios = ?,driver_min_ver_ios = ?,rider_min_ver_android = ?,driver_min_ver_android = ? WHERE id = ?"
            , [
                data.max_drivers,
                data.max_distance,
                data.minimum_payment_request,
                data.percent_company,
                data.cash_payment_commission,
                data.rider_min_ver_ios,
                data.driver_min_ver_ios,
                data.rider_min_ver_android,
                data.driver_min_ver_android,
                id[0].id
            ]);
        let [res, ignor] = await sql.query("SELECT * FROM general_settings order by id desc limit 1");
        return res[0];
    }
};