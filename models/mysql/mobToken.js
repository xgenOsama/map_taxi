module.exports = {
    getAll: async function (userId) {
        let [result, ignored] = await sql.query("SELECT * FROM mobile_tokens WHERE driver_id = ? limit 1", [userId]);
        //result.map(x=>x.location = [x.location.y,x.location.x]);
        return result;
    },
    getAllFirebase: async function () {
        let [result, ignored] = await sql.query("SELECT token_firebase FROM mobile_tokens");
        return result;
    },
    getTokens: async function (userId) {
        let [result, ignored] = await sql.query("SELECT mobile_token , token_firebase FROM mobile_tokens WHERE driver_id = ?", [userId]);
        return result;
    },
    getAndroidToken: async function(userId){
        let [result, ignored] = await sql.query("SELECT mobile_token, token_firebase FROM mobile_tokens WHERE driver_id = ? AND device = ? limit 1", [userId,'android']);
        //result.map(x=>x.location = [x.location.y,x.location.x]);
        console.log(result[0]);
	return result;
    },
    getIosToken: async function(userId){
        let [result, ignored] = await sql.query("SELECT mobile_token, token_firebase FROM mobile_tokens WHERE driver_id = ? AND device = ? limit 1", [userId,'ios']);
        //result.map(x=>x.location = [x.location.y,x.location.x]);
        return result;
    },
    checkHaveAndroid: async function(userId){
        let [result, ignored] = await sql.query("SELECT * FROM mobile_tokens WHERE driver_id = ? AND device = ? limit 1", [userId,'android']);
        return result;
    },
    checkHaveIos: async function(userId){
        let [result, ignored] = await sql.query("SELECT * FROM mobile_tokens WHERE driver_id = ? AND device = ? limit 1", [userId,'ios']);
        return result;
    },
    updateToken: async function(userId,token){
        let [result, ignored] = await sql.query("UPDATE mobile_tokens SET mobile_token = ? WHERE id = ?", [token,userId]);
        return true;
    },
    insert: async function (token) {
        let [result, ignored] = await sql.query("INSERT INTO mobile_tokens (device, mobile_token, driver_id, token_firebase) VALUES (?, ?, ?,?)", [token.device, token.mobile_token, token.driver_id,token.token_firebase]);
        return true;
    },
    update: async function (token) {
        let [result, ignored] = await sql.query("UPDATE mobile_tokens SET device = ?, mobile_token = ?, driver_id = ?, token_firebase = ?  WHERE id = ?", [token.device, token.mobile_token, token.driver_id, token.token_firebase, token.id]);
        return true;
    },
    delete: async function (token) {
        let [result, ignored] = await sql.query("DELETE FROM mobile_tokens WHERE id = ?", [token.id]);
        return true;
    }
};
