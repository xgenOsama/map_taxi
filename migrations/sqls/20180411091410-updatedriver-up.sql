ALTER TABLE `driver` ADD COLUMN `city` varchar(60) NULL DEFAULT NULL after `address`,
    ADD COLUMN `car_insurance_photo_id` int(11) NULL DEFAULT NULL after `address`,
    ADD COLUMN `car_license_photo_id`  int(11) NULL DEFAULT NULL after `address`,
    ADD COLUMN `driver_identity_photo_id`  int(11) NULL DEFAULT NULL after `address`,
    ADD COLUMN `driver_license_photo_id`  int(11) NULL DEFAULT NULL after `address`;

ALTER TABLE `driver` ADD CONSTRAINT `fk_car_insurance_photo_id` FOREIGN KEY (`car_insurance_photo_id`) REFERENCES `media` (`id`) ON DELETE SET NULL ON UPDATE CASCADE ;
ALTER TABLE `driver` ADD CONSTRAINT `fk_car_license_photo_id` FOREIGN KEY (`car_license_photo_id`) REFERENCES `media` (`id`) ON DELETE SET NULL ON UPDATE CASCADE ;
ALTER TABLE `driver` ADD CONSTRAINT `fk_driver_identity_photo_id` FOREIGN KEY (`driver_identity_photo_id`) REFERENCES `media` (`id`) ON DELETE SET NULL ON UPDATE CASCADE ;
ALTER TABLE `driver` ADD CONSTRAINT `fk_driver_license_photo_id` FOREIGN KEY (`driver_license_photo_id`) REFERENCES `media` (`id`) ON DELETE SET NULL ON UPDATE CASCADE ;

ALTER TABLE
    `media`
MODIFY COLUMN
    `type` enum(
    'car',
    'service',
    'driver image',
    'driver header',
    'operator image',
    'rider image',
    'car_insurance_photo',
    'car_license_photo',
    'driver_identity_photo',
    'driver_license_photo'
    )
DEFAULT NULL AFTER `address`;
